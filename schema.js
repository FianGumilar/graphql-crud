const { gql } = require('apollo-server');

exports.typeDefs = gql`
    type Query {
      hello: [String!]
      products(filter: ProductsFilterInput): [Product!]!
      product(id: ID!): Product
      categories: [Category!]!
      category(id: ID!): Category
      review(id: ID!): Review
      reviews: [Review!]!
    }

    type Mutation {
      addCategory(input: addCategoryInput!): Category!
      addProduct(input: addProductInput!): Product!
      addReview(input: addReviewInput!): Review!
      deleteCategory(id: ID!): Boolean!
      deleteProduct(id: ID!): Boolean!
      deleteReview(id: ID!): Boolean!
      updateCategory(id: ID!, input: updateCategoryInput!): Category
      updateProduct(id: ID!, input: updateProductInput!): Product
      updateReview(id: ID!, input: updateReviewInput!): Review
    } 

    type Product {
      id: ID!
      name: String!
      description: String!
      image: String!
      quantity: Int!
      price: Float!
      onSale: Boolean!
      categoryId: String!
      category: Category
      reviews: [Review!]!
    }

    type Category {
      id: ID!
      name: String!
      products(filter: ProductsFilterInput): [Product!]!
    }

    type Review {
      id: ID!
      date: String!
      title: String!
      comment: String!
      rating: Int!
    }

    input ProductsFilterInput {
      onSale: Boolean
      avgRating: Int
    }

    input addCategoryInput {
      name: String!
    }

    input updateCategoryInput {
      name: String!
    }

    input addProductInput {
      name: String!
      description: String!
      image: String!
      quantity: Int!
      price: Float!
      onSale: Boolean!
      categoryId: String!
    }

    input updateProductInput {
      name: String
      description: String
      image: String
      quantity: Int
      price: Float
      onSale: Boolean
      categoryId: String
    }

    input addReviewInput {
      title: String!
      comment: String!
      rating: Int!
      productId: String!
    }

    input updateReviewInput {
      title: String
      comment: String
      rating: Int
      productId: String
    }

`