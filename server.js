const { ApolloServer, gql } = require('apollo-server');
const { typeDefs} = require('./schema');
const { Category } = require('./resolvers/category');
const { Mutation } = require('./resolvers/mutation');
const { Query } = require('./resolvers/query');
const { Product } = require('./resolvers/product');
const { db} = require('./db');

const server = new ApolloServer({
    typeDefs,
    resolvers: {
      Query,
      Product,
      Category,
      Mutation
    },
    context: {
      db
    }
});

server.listen().then(({url}) => {
    console.log('Listening on ' + url);
});